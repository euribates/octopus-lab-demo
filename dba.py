#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3
import rsa
from hashlib import md5
from config import SALT

_db_handle = None
def connect():    
    global _db_handle
    if not _db_handle:
        _db_handle = sqlite3.connect('database.db')
    return _db_handle


def hash_word(word):
    word = str(word).encode('utf-8')
    return md5(SALT+word).hexdigest()


def load_public_key():
    with open('public_key.pem', 'r') as f:
        public_key = rsa.PublicKey.load_pkcs1(f.read(), 'PEM')
        return public_key


def load_private_key():
    with open('private_key.pem', 'r') as f:
        private_key = rsa.PrivateKey.load_pkcs1(f.read(), 'PEM')
        return private_key


def encrypt_word(word):
    word = str(word).encode('utf-8')
    pub_key = load_public_key()
    return rsa.encrypt(word, pub_key)


def decrypt_word(word):
    prv_key = load_private_key()
    word = rsa.decrypt(word, prv_key)
    return word.decode('utf-8')


def update_sentiment(conn, url, sentiment):
    cur = conn.cursor()
    id_page = hash_word(url) 
    try:
        sql = 'Update page set sentiment=? where id_page=?'
        cur.execute(sql, (sentiment, id_page))
    finally:
        cur.close()
        conn.commit()




def add_page(conn, url, title, sentiment=0):
    cur = conn.cursor()
    id_page = hash_word(url) 
    try:
        cur.execute('Select id_page from page where id_page=?', (id_page,))
        row = cur.fetchone()
        if row:
            sql = 'Update page set title=?, sentiment=? where id_page=?'
            cur.execute(sql, (title, sentiment, id_page))
        else:
            sql = 'Insert into page'  \
                  ' (id_page, title, url, sentiment)' \
                  ' values(?, ?, ?, ?)'
            cur.execute(sql, (id_page, title, url, sentiment))
        return id_page
    finally:
        cur.close()
        conn.commit()


def add_word(conn, word, counter):
    id_word = hash_word(word)
    cur = conn.cursor()
    try:
        cur.execute('Select id_word from word where id_word=?', (id_word,))
        row = cur.fetchone()
        if row:
            sql = 'Update word set counter = counter + ? where id_word = ?' 
            cur.execute(sql, (counter, id_word))
        else:
            sql = 'Insert into word (id_word, word, counter) values(?, ?, ?)'
            cur.execute(sql, (id_word, encrypt_word(word), counter))
        return id_word
    finally:
        cur.close()
        conn.commit()


def add_words(conn, words):
    conn = connect()
    for word, counter in words:
        add_word(conn, word, counter)


def get_all_words(conn):
    cur = conn.cursor()
    try:
        cur.execute(
            'SELECT id_word, word, counter'
            '  FROM word'
            ' ORDER BY counter desc'
            )
        result = [
            dict(
                id_word=r[0],
                word=decrypt_word(r[1]),
                counter=r[2]
                )
            for r in cur.fetchall()
            ]
        return result
    finally:
        cur.close()


def get_all_pages(conn):
    cur = conn.cursor()
    try:
        cur.execute(
            'SELECT id_page, url, title, sentiment'
            '  FROM page'
            ' ORDER BY title'
            )
        result = [
            dict(
                id_page=r[0],
                url=r[1],
                title=r[2],
                sentiment=r[3]
                )
            for r in cur.fetchall()
            ]
        return result
    finally:
        cur.close()






