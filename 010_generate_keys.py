#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import rsa

if any([os.path.exists('public_key.pem'), os.path.exists('private_key.pem')]):
    print('A pair of public/private keys exists.')
    print('Remove this files to create a new pair.')
else:
    (pubkey, privkey) = rsa.newkeys(512)
    with open('public_key.pem', 'wb') as f:
        f.write(pubkey.save_pkcs1('PEM'))
    with open('private_key.pem', 'wb') as f:
        f.write(privkey.save_pkcs1('PEM'))
    print('Public/private keys created')

