#!/usr/bin/env python

PORT = 8888

MAX_WORDS = 100

STATIC_PATH = './static'

SALT = b'Octopus'
