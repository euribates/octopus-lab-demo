## OctupusLab demo Readme

### About the code chalenge

First of all, thank you for this code challenge. It's a very
interesting problem, with a lot of technologies involved. I really
enjoyed working on it.

Unfortunately, I couldn't make all the points. Specifically,
that is what it is done:

#### Thigs Done

 - Web server on python 3/Tornado

 - Application in a docker container (but not in docker compose)

 - Application does all the things specified:

   - Extract the words of the page indicated by the URL

   - Count every word apparence

   - The 100th firsts word, according to number of repetitions
     are stored in a table. Keys are generated using a hash
     plus salt. Words are encripted using an asimmetrycal 
     encryptation algorythm (RSA).

   - Pages are stored in a database table, too. I store the
     id of every page, again a combination of salt+hast, the
     URL and title of the page, and a indicator of sentiment
     analysis.
     
   - The sentiment analysis use the WIT.io api. This api give a
     labeleld result with values `positive` or `negative`, and
     a real value in the range [0..1] as relevance. I opted for
     storing a float or real value in the range [-1, 1]. Values 
     near -1 are to be considered negative, values around zero are 
     neutral and values near the positive one are considered positives.

   - The most frequents words are displayed using a "word cloud",
     using javascript on client and the D3 library.

   - There are options in the top bar to see the admin pages
     of words and pages analyzed, es requested.


#### Things not acomplished

Now, on the things I didn't finish:

  - I don't use a MySql database, but sqlite. This is
  completely my fault: I read the problem and make this
  mistake, (probably because of the rush) and only
  noticed my error until five or six hours after
  I started working. I decided to finish with sqlite
  and later try to adapt to mysql, but I ran out of time
  (Nevertheless, all the code involved is on one single
  module, `dba.py`).

  - I didn't use docker composer. I understand that the
  idea is run two containers, one with the web server and
  other with the database, but again I run out of time.
  Docker is a new technology for me.

#### Things that could be improved

Now, on things that works but I'm not pleased:

  - I have not worked with Tornado before, but I understand
   that my code is far, far from optimal, because I barely
   use the async philosophy of tornado. I made async calls
   to fetch the content of the URL, but the database operations
   are not async, so doesn't fit well with the system. If I had 
   used and async mysql driver I assume the code will 
   suit better.

  - I do the call to WIT.io API as a task apart, so the
  sentiment analysis is made as a _run-and-forget_ task, using
  `ioloop.IOLoop.current().spawn_callback`. I'm not sure
  this is the better method.

  - The code that cleans the HTML could be improved.

  - The sentiment analysis itself doesn't work well with
    the pages I tried. Usually returns a error code, specially
    if the text is huge. I didn't find any comment on the 
    documentation about size limits. As the API worked
    better with small pieces of text, I made a hack
    to create a fake summary of the page, taking just the 40
    more used words in the text. Maybe using one API
    specializad in sentiment analysis like 
    [Watson's Tone Analize](https://www.ibm.com/watson/services/tone-analyzer/)
    that size limit colud be overcome.

    I tuned some pages form the mobile version of the Wikipedia
    to get high values, either positive or negative. The links
    are in the homapage.


### About the installation with Docker 

To create the image of the webserver, first we need to install docker

    sudo apt-get install docker.io

And pull the imagenes we'll need, `mysql` and `python`:

    docker pull mysql
    docker pull python

We can check the python version:

    docker run --rm python python -V

And we can run the mysql image:

    docker run --rm --name mysqldb -e MYSQL_ROOT_PASSWORD=octopus -d mysql:latest

In the docker image, we will use pipenv to get a private environment

    pipenv install --three
    pipenv shell
    pipenv install tornado
    pipenv install pytest, pytest-sugar
    pipenv install wit
    pipenv install bs4 
    pipenv install rsa

all of this pipenv operations store the dependencies on the files `Pipfile` and
`Pipfile.lock`, so there is no need to make the install of every package, a simple
pipenv install make all the operations for us.

- lib `tornado` is the framework

- lib `pytest` and related are testing utilities

- lib `rsa` is used to generate a pair of keys for asymmetric encryption, to
  save the word cyphered

- lib `wit` is a python interface for the WIT.io API.

- lib `bs4` is also known as *ButterflySoup* and is used to clean 
  the html tags of the downloaded pages. We can select just the body 
  part, and BautifulSoup has a function called `get_text` that gives 
  us just the bare words.

### Create the Docker image

To create the docker image, run the command on the same directory
where is located the `Dockerfile`:

    docker build -t pythonweb .

### Run the Image

To run the image created en the previous step, do:

    docker run --rm -p 8888:8888 pythonweb

If all has gone well, you can open a web browser to 
[http://localhost:8888](http://localhost:8888)
and see the homepage of the application.

Thank you for this opportunity and greetings from Canary Islands.


#### Other notes

The list of the most common words to exclude came from this page:
<https://simple.wikipedia.org/wiki/Most_common_words_in_English>
