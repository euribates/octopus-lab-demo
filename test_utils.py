#!/usr/bin/env python

import pytest
import utils


def test_simple_paragraph():
    source = '''<html>
<head>
    <title>hello</title>
</head>
<body>
    <p>Hello, World.</p>
</body>
</html>'''
    title, body_text = utils.clean_html(source)
    assert title == 'hello'
    assert body_text == 'Hello, World.'



def test_count_words():
    text = ('Mary had a little lamb\n'
            'Little lamb, little lamb\n'
            'Mary had a little lamb\n'
            'Its fleece was white as snow\n'
            'And everywhere that Mary went\n'
            'Mary went, Mary went\n'
            'Everywhere that Mary went\n'
            'The lamb was sure to go')
    counts = utils.count_words(text, exclude=[])
    print(counts)
    assert counts['mary'] == 6
    assert counts['little'] == 4
    assert counts['sure'] == 1
    assert counts['white'] == 1
    assert counts['snow'] == 1
    assert counts['a'] == 2
    assert counts['octopus'] == 0


