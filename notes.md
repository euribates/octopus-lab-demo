detach from an interactive docker:

Yes, we can detach it from our container by using the Ctrl + P and Ctrl +
Q escape sequence. This escape sequence will detach the TTY from the container and
land us in the Docker host prompt $ , however the container will continue to run.


## Dockerfile

Dockerfile is a text-based build script that contains special instructions in a
sequence for building the right and relevant images from the base images. 

The sequential instructions inside the Dockerfile can include the base image
selection, installing the required application, adding the configuration and
the data files, and automatically running the services as well as exposing
those services to the external world.

The Docker engine tightly integrates this build process with the help of the docker
build subcommand

    docker build -t busyboxhello .

To add a tag to a image (if we forget to use the `-t` flag)

    docker tag af103a7baec7 busyboxhello 

### Comands you can use inside a Dockerfile

#### FROM

Used to specify the base image that will be used to build the new one

    FROM <image>[:<tag>]

By default, the docker build system looks in the Docker host for the images. 
However, if the image is not found in the Docker host, then the docker 
build system will pull the image from the publicly available Docker Hub Registry.

#### MAINTAINER

add information about the mainteiner of ths image. Usually placed after the
`FROM`

    MAINTAINER <author details>

#### COPY

The COPY instruction enables you to copy the files from the Docker host to the
filesystem of the new image. The following is the syntax of the COPY instruction:

    COPY <src> ... <dst>

#### ADD

The ADD comend is similar to COPY, but it allows to copy a tree of
subdirectories. 
