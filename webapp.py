#!/usr/bin/env python

import tornado
from tornado import web
from tornado import ioloop

from jinja2 import Environment, FileSystemLoader
from utils import  clean_html, extract_words, count_words, fetch_coroutine
import dba

from config import PORT, MAX_WORDS, STATIC_PATH


env = Environment(loader=FileSystemLoader('./templates'))


class CloudHandler(web.RequestHandler):

    def get(self):
        template = env.get_template('cloud.html')
        self.write(template.render(
            title='Words found - OctopusLab Demo',
            ))


class PagesHandler(web.RequestHandler):

    def get(self):
        template = env.get_template('pages.html')
        conn = dba.connect()
        pages = dba.get_all_pages(conn)
        self.write(template.render(
            title='Analyzed pages - OctopusLab Demo',
            pages=pages,
            num_pages =len(pages),
            ))


class WordsHandler(web.RequestHandler):

    def get(self):
        template = env.get_template('words.html')
        conn = dba.connect()
        words = dba.get_all_words(conn)
        self.write(template.render(
            title='Cloud test - OctopusLab Demo',
            words=words,
            num_words=len(words),
            ))


def check_sentiment(url, text):
    print('check_sentiment starts')
    print('url:', url)
    import wit
    client = wit.Wit('2M2MDXFXI6SAYTJ424TESKSYB6YNX6NS')
    result = client.message(text)
    print(type(result), result)
    if 'entities' in result:
        entities = result['entities']
        print(type(entities), entities)
        if 'sentiment' in entities:
            sentiment = entities['sentiment']
            print(type(sentiment), sentiment)
            acc = []
            for item in sentiment:
                if item['value'] == 'negative':
                    value = - item['confidence']
                else:
                    value = item['confidence']
                acc.append(value)
            sentiment = sum(acc) / len(acc)
            print('sentiment:', sentiment)
            conn = dba.connect()
            dba.update_sentiment(conn, url, sentiment)


class MainHandler(web.RequestHandler):

    def get(self):
        template = env.get_template('index.html')
        self.write(template.render(
            title='OctopusLab Demo',
            ))

    async def post(self):
        url = self.get_argument('url')
        text = await fetch_coroutine(url)
        title, plain_text = clean_html(text)
        words = extract_words(plain_text)
        words = count_words(words).most_common(MAX_WORDS)
        summary = ' '.join([_[0] for _ in words[0:40]]) # 40 most used words
        ioloop.IOLoop.current().spawn_callback(
                check_sentiment, 
                url,
                summary,
                )

        conn = dba.connect()

        dba.add_page(conn, url, title)
        dba.add_words(conn, words)
        template = env.get_template('response.html')
        self.write(template.render(
            title='Thank you! - OctopusLab Demo',
            url=url,
            words=words,
            ))

def make_app():
    return web.Application([
        (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': STATIC_PATH}),
        (r'/', MainHandler),
        (r'/words/', WordsHandler),
        (r'/pages/', PagesHandler),
        (r'/cloud/', CloudHandler),
        ])

if __name__ == '__main__':
    app = make_app()
    print('Waiting petitions on port {}'.format(PORT), end='')
    app.listen(PORT)
    print('[OK]')
    ioloop.IOLoop.current().start()
