#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import rsa
import dba


def test_add_page():
    conn = dba.connect()
    dba.add_page(conn, 'https://www.bbc.com/news', 'Home - BBC News')


def test_hash_word():
    assert dba.hash_word('spider') == '62da7b0a111a275cb095463e7aa74d6b'


def test_load_public_key():
    pub_key = dba.load_public_key()
    assert isinstance(pub_key, rsa.PublicKey)


def test_load_private_key():
    prv_key = dba.load_private_key()
    assert isinstance(prv_key, rsa.PrivateKey)


def test_encrypt_word():
    cyphered =  dba.encrypt_word('spider')
    assert dba.decrypt_word(cyphered) == 'spider'
