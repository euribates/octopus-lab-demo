#!/usr/bin/env python

import re
import bs4
from collections import Counter
from tornado.httpclient import AsyncHTTPClient


pat_html_comment = re.compile('<!--.+-->')
def clean_html(s):
    soup = bs4.BeautifulSoup(s, "html.parser")
    title = soup.head.title.get_text()
    for script in soup(["script", "style"]): # remove javascript/stylesheets
        script.extract()
    text = soup.body.get_text()
    text = pat_html_comment.sub(' ', text)
    lines = (line.strip() for line in text.splitlines() if line.strip())
    plain_text = '\n'.join(lines)
    with open('ejemplo.txt', 'w') as f:
        f.write(plain_text)
    return title, plain_text


_exclude_words = [ '',
    'the', 'be', 'to', 'of', 'and', 'a', 'in', 'that', 'have', 'i', 
    'it', 'for', 'not', 'on', 'with', 'he', 'as', 'you', 'do', 'at', 
    'this', 'but', 'his', 'by', 'from', 'they', 'we', 'say', 'her', 'she', 
    'or', 'will', 'an', 'my', 'one', 'all', 'would', 'there', 'their', 
    'what', 'so', 'up', 'out', 'if', 'about', 'who', 'get', 'which', 
    'go', 'when', 'me', 'make', 'can', 'like', 'time', 'no', 'just', 
    'him', 'know', 'take', 'person', 'into', 'year', 'your', 'good', 'some', 
    'could', 'them', 'see', 'other', 'than', 'then', 'now', 'look', 'only', 
    'come', 'its', 'over', 'think', 'also', 'back', 'after', 'use', 'two', 
    'how', 'our', 'work', 'first', 'well', 'way', 'even', 'new', 'want', 
    'because', 'any', 'these', 'give', 'day', 'most', 'us',
    ]
    

_pat_sep = re.compile('[^A-Za-z]+')
def extract_words(text, exclude=_exclude_words):
    global _pat_sep
    if exclude is None:
        exclude = []
    return [
        w.lower()
        for w in _pat_sep.split(text)
        if w.lower() not in exclude
        ]


def count_words(words):
    return Counter(words)


async def fetch_coroutine(url):
    http_client = AsyncHTTPClient()
    response = await http_client.fetch(url)
    return response.body



