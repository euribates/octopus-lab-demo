#!/usr/bin/env python

import time
import sys
from tornado import gen
from tornado import ioloop
from tornado.httpclient import AsyncHTTPClient
from utils import  clean_html, count_words, fetch_coroutine


async def process(url):
    text = await fetch_coroutine(url)
    title, plain_text = clean_html(text)
    return count_words(plain_text)

if __name__ == '__main__':
    url = 'https://en.wikipedia.org/wiki/Crambus_melanoneurus'
    #url = 'https://www.bbc.com/news/science-environment-44521599'
    s = ioloop.IOLoop.current().run_sync(lambda: process(url))
    print(s.most_common(10))
