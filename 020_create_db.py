#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

conn = sqlite3.connect('database.db')
c = conn.cursor()

# Create table PAGE
c.execute('''
CREATE TABLE IF NOT EXISTS page (
    id_page text primary key, 
    url text,
    title text, 
    sentiment real
    )
''')

# Create table WORD
c.execute('''
CREATE TABLE IF NOT EXISTS word (
    id_word text primary key, 
    word text, 
    counter integer
    )
''')

