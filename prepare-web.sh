git clone https://euribates@bitbucket.org/euribates/octopus-lab-demo.git
cd octopus-lab-demo
pip install pipenv
pipenv install 
pipenv check
pipenv run ./010_generate_keys.py
pipenv run ./020_create_db.py
