FROM python:latest
MAINTAINER Juan Ignacio Rodríguez de León <euribates@gmail.com>
EXPOSE 8888/tcp
ADD . /code
WORKDIR /code
RUN /code/prepare-web.sh
ENTRYPOINT /code/entrypoint-web.sh


